#ifndef TASK_0_PARSER_OF_FILE_H
#define TASK_0_PARSER_OF_FILE_H

#include <fstream>
#include <algorithm>
#include <string>
#include <map>

#include "errors.h"
#include "const.h"
using std::wifstream;
using std::wstring;
using std::string;
using std::pair;
using std::map;
namespace program
{
    namespace reader
    {
        class Reader
        {
        private:
            map<wstring,int> words;
            long long counterWords = 0;
            void readWordsFromString(wstring str, program::consts::Case letterCase);
        public:
            map<wstring,int> getMap() const;
            long long getCounter() const;
            void readWords(wifstream &in, program::consts::Case letterCase);
        };
    }
}
#endif //TASK_0_PARSER_OF_FILE_H

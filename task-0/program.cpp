#include "program.h"

void program::mainProgram::Program::readParameters(int argc, char **argv)
{
    if(argc != (consts::rightNumberOfParameters))
    {
        throw program::exceptions::WrongNumberParameter();
    }
    parserCase(argv);
    in.open(argv[consts::numberInputFile], std::ios::in | std::ios::binary);
    out.open(argv[consts::numberOutputFile], std::ios::out | std::ios::binary);
    if(!in || !out)
        throw program::exceptions::OpenFile();
}

void program::mainProgram::Program::read(program::reader::Reader reader)
{
    reader.readWords(in, letterCase);
    counterWords = reader.getCounter();
    words = reader.getMap();
}

void program::mainProgram::Program::sort(program::sort::SortWords &sorting)
{
    sorting.setWords(words);
    listOfSortWords = sorting.getSortedList();
}

void program::mainProgram::Program::print(program::printer::Printer printer)
{
    printer.setCounter(counterWords);
    printer.setList(listOfSortWords);
    printer.printWords(out);
}

void program::mainProgram::Program::parserCase(char **argv)
{
   if(argv[consts::numberLetterCase] == enum_map[consts::CaseSensitive])
   {
       letterCase = consts::CaseSensitive;
   }
   else
   {
       letterCase = consts::CaseInsensitive;
   }
}
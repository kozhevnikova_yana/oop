#ifndef TASK_0_CONST_H
#define TASK_0_CONST_H

#include <map>
#include <string.h>
namespace program
{
    namespace consts
    {
        enum Case {CaseInsensitive = 0, CaseSensitive = 1};
        const int numberLetterCase = 1;
        const int numberInputFile = 2, numberOutputFile = 3;
        const int rightNumberOfParameters = 4;
    }
}
#endif //TASK_0_CONST_H

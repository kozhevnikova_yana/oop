#include "print.h"

void program::printer::Printer::setList(const std::list<pair<int,wstring>> &listOfSortWords)
{
    this->listOfSortWords = listOfSortWords;
}

void program::printer::Printer::setCounter(const long long &counter)
{
    this->counterWords = counter;
}

void program::printer::Printer::printWords(std::wofstream &out)
{
    for(auto ix=listOfSortWords.begin(); ix!=listOfSortWords.end(); ++ix)
    {
        double freq= ix->first*100.0/counterWords;
        out<<ix->second<<";"<<ix->first<<";"<<std::fixed<<std::setprecision(8)<<freq<<"%;\r\n";
    }
    listOfSortWords.clear();
    out.close();
}
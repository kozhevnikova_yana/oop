#ifndef TASK_0_PRINT_H
#define TASK_0_PRINT_H

#include <iostream>
#include <fstream>
#include <string>
#include <iomanip>
#include <map>
#include <list>

#include "parser_of_file.h"
#include "sort.h"
namespace program
{
    namespace printer
    {
        class Printer
        {
        private:
            std::list<pair<int,wstring>> listOfSortWords;
            long long int counterWords;
        public:
            void setList(const std::list<pair<int,wstring>> &listOfSortWords);
            void setCounter(const long long &counter);
            void printWords(std::wofstream &out);
        };
    }
}
#endif //TASK_0_PRINT_H

#include <fstream>
#include <string>
#include <locale.h>
#include <windows.h>

#include "program.h"

int main(int argc, char * argv[])
{
    SetConsoleCP(1251);
    SetConsoleOutputCP(1251);
    setlocale(0, "");

    program::mainProgram::Program program;
    program::reader::Reader reader;
    program::sort::SortWords sorting;
    program::printer::Printer printer;
    try {
        program.readParameters(argc, argv);
        program.read(reader);
        program.sort(sorting);
        program.print(printer);
    }
    catch (program::exceptions::WrongNumberParameter &e) {
        std::cerr << e.what();
    }
    catch (program::exceptions::OpenFile &e) {
        std::cerr << e.what();
    }
    return 0;
}
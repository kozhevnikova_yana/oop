#include "errors.h"

const char * program::exceptions::WrongNumberParameter::what() const noexcept
{
    return "You must write 3 parameters. You didn't do this.";
}

const char * program::exceptions::OpenFile::what() const noexcept
{
    return ("I can't open input or output file. Please, check names of file.");
}

#ifndef TASK_0_PROGRAM_H
#define TASK_0_PROGRAM_H

#include <fstream>

#include "errors.h"
#include "parser_of_file.h"
#include "sort.h"
#include "print.h"
#include "errors.h"
namespace program
{
    namespace
    {
        std::map<int, std::string> enum_map = {{consts::CaseInsensitive, "CaseInsensitive"},
                                               {consts::CaseSensitive, "CaseSensitive"}};
    }
    namespace mainProgram
    {
        class Program
        {
        private:
            std::wifstream in;
            std::wofstream out;
            consts::Case letterCase;
            long long int counterWords;
            map<wstring,int> words;
            std::list<pair<int,wstring>> listOfSortWords;
            void parserCase(char **argv);
        public:
            void readParameters(int argc, char * argv[]);
            void read(program::reader::Reader reader);
            void sort(program::sort::SortWords &sorting);
            void print(program::printer::Printer printer);
        };
    }
}
#endif //TASK_0_PROGRAM_H

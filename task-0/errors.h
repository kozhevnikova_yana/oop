#ifndef TASK_0_ERRORS_H
#define TASK_0_ERRORS_H

#include <iostream>
#include <exception>
#include <stdexcept>
#include <string>

namespace program
{
    namespace exceptions
    {
        class WrongNumberParameter: public std::exception
        {
        public:
            const char *what() const noexcept;
        };

        class OpenFile : public std::exception
        {
        public:
            const char *what()  const noexcept;
        };

    }
}
#endif //TASK_0_ERRORS_H

#include "parser_of_file.h"
map<wstring,int> program::reader::Reader::getMap() const
{
    return words;
}
long long program::reader::Reader::getCounter() const
{
    return counterWords;
}
void program::reader::Reader::readWordsFromString(wstring str, program::consts::Case caseLetters)
{
    wstring::iterator pfirst = str.begin();
    wstring::iterator plast = str.end();
    while (pfirst != plast)
    {
        wstring wordtext(L"");
        while ((pfirst != plast) && (!iswalnum(*pfirst)))
            ++pfirst;
        while ((pfirst != plast) && (iswalnum(*pfirst)))
            wordtext += *(pfirst++);
        if(!caseLetters)
        {
            transform(wordtext.begin(),wordtext.end(),wordtext.begin(), towlower);
        }
        if (!wordtext.empty())
        {
            ++counterWords;
            auto result = words.find(wordtext);
            if(result != words.end())
                ++words[wordtext];
            else
                words.insert( pair<wstring,int>(wordtext,1));
        }
    }
}
void program::reader::Reader::readWords(wifstream &in, program::consts::Case letterCase)
{
    wstring buffer;
    while (getline(in,buffer))
    {
        readWordsFromString(buffer,letterCase);
    }
    in.close();
}
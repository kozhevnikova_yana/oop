#include "sort.h"

void program::sort::SortWords::setWords(const map<wstring,int> &words)
{
    this->words = words;
}
void program::sort::SortWords::copyWordToList()
{
    for(auto it = words.begin(); it != words.end(); ++it) {
        pair<int,wstring> ourWord = make_pair(it->second,it->first);
        listOfSortWords.push_back(ourWord);
    }
}

void program::sort::SortWords::sortListWords()
{
    listOfSortWords.sort();
    listOfSortWords.reverse();
}

std::list<pair<int,wstring>> program::sort::SortWords::getSortedList()
{
    program::sort::SortWords::copyWordToList();
    program::sort::SortWords::sortListWords();
    return listOfSortWords;
}

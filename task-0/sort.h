#ifndef TASK_0_SORT_H
#define TASK_0_SORT_H

#include <map>
#include <list>
#include <string>

#include "parser_of_file.h"
namespace program
{
    namespace sort
    {
        class SortWords
        {
        private:
            map<wstring,int> words;
            std::list<pair<int,wstring>> listOfSortWords;
            void copyWordToList();
            void sortListWords();
        public:
            void setWords(const map<wstring,int> &words);
            std::list<pair<int,wstring>> getSortedList();
        };
    }
}
#endif //TASK_0_SORT_H

#include <fstream>
#include "gtest/gtest.h"
#include "../Trit.h"
#include "../Tritset.h"

TEST (trit_tests, create)
 {
    ASSERT_EQ (Trit().value(), trit::Unknown);
    ASSERT_EQ (Trit(trit::True).value(), trit::True);
    Trit tmp(trit::False);
    ASSERT_EQ (Trit(tmp).value(), trit::False);
}

TEST (trit_tests, compare) 
{
    Trit t(trit::True), f(trit::False), u(trit::Unknown);
    ASSERT_TRUE(t == t);
    ASSERT_TRUE(f == f);
    ASSERT_TRUE(u == u);
    ASSERT_TRUE(t != f);
    ASSERT_TRUE(u != t);

}

TEST (trit_tests, logical_and) 
{
    Trit t(trit::True), f(trit::False), u(trit::Unknown);
    ASSERT_EQ (f & f, f);
    ASSERT_EQ (f & t, f);
    ASSERT_EQ (f & u, f);
    ASSERT_EQ (u & f, f);
    ASSERT_EQ (u & u, u);
    ASSERT_EQ (u & t, u);
    ASSERT_EQ (t & t, t);
    ASSERT_EQ (t & f, f);
    ASSERT_EQ (t & u, u);
}

TEST (trit_tests, logical_or) 
{
    Trit t(trit::True), f(trit::False), u(trit::Unknown);
    ASSERT_EQ (f | f, f);
    ASSERT_EQ (f | t, t);
    ASSERT_EQ (f | u, u);
    ASSERT_EQ (u | f, u);
    ASSERT_EQ (u | u, u);
    ASSERT_EQ (u | t, t);
    ASSERT_EQ (t | t, t);
    ASSERT_EQ (t | u, t);
    ASSERT_EQ (t | f, t);
}

TEST (trit_tests, logical_not)
{
    Trit t(trit::True), f(trit::False), u(trit::Unknown);
    ASSERT_EQ(!t, f);
    ASSERT_EQ(!f, t);
    ASSERT_EQ(!u, u);
}

TEST (trit_tests, assign) 
{
    Trit tmp(trit::True);
    tmp &= Trit(trit::False);
    ASSERT_EQ (tmp.value(), trit::False);
    tmp |= Trit(trit::False);
    ASSERT_EQ (tmp.value(), trit::False);
    tmp = Trit();
    ASSERT_EQ (tmp.value(), trit::Unknown);
}

TEST (tritset_tests, printf) 
{
    Tritset set(10, Trit(trit::True));
    std::string str = set.to_string();
    ASSERT_EQ(str, "1111111111??????");
}

TEST (tritset_tests, constructors)
 {
    Tritset set(20, Trit(trit::True));
    ASSERT_EQ(set.to_string(), "11111111111111111111????????????");
    Tritset set2(10, Trit(trit::True));
    ASSERT_EQ(set2.to_string(), "1111111111??????");
    Tritset set3(10);
    ASSERT_EQ(set3.to_string(), "????????????????");
}

TEST (tritset_tests, counters_length)
 {
    ASSERT_EQ(Tritset(20, Trit(trit::True)).length(), 20);
    ASSERT_EQ(Tritset(10, Trit(trit::False)).length(), 10);
    ASSERT_EQ(Tritset(10).length(), 0);
}

TEST (tritset_tests, counters_capacity) 
{
    ASSERT_EQ(Tritset(20, Trit(trit::True)).capacity(), 32);
    ASSERT_EQ(Tritset(10, Trit(trit::False)).capacity(), 16);
    ASSERT_EQ(Tritset(10).capacity(), 16);
}

TEST (tritset_tests, counters_size)
 {
    ASSERT_EQ(Tritset(20, Trit(trit::True)).size(), 2);
    ASSERT_EQ(Tritset(10, Trit(trit::False)).size(), 1);
    ASSERT_EQ(Tritset(10).size(), 1);
}

TEST (tritset_tests, assignments)
 {
    Tritset set(20, Trit(trit::True));
    set[0] = Trit(trit::False);
    ASSERT_EQ(set.to_string(), "01111111111111111111????????????");
    set[0] = Trit();
    ASSERT_EQ(set.to_string(), "?1111111111111111111????????????");
    set[31] = Trit();
    ASSERT_EQ(set.length(), 20);
    set[31] = Trit(trit::True);
    ASSERT_EQ(set.length(), 32);
    set[32] = Trit();
    ASSERT_EQ(set.capacity(), 32);
    set[32] = Trit(trit::True);
    ASSERT_EQ(set.capacity(), 48);
}

TEST (tritset_tests, counters_cardinality) 
{
    Tritset set(20, Trit(trit::True));
    ASSERT_EQ(set.cardinality(Trit(trit::True)), 20);
    ASSERT_EQ(set.cardinality(Trit(trit::False)), 0);
    ASSERT_EQ(set.cardinality(Trit()), 0);

    set[2] = trit::Unknown;
    set[6] = trit::Unknown;
    set[0] = trit::False;
    ASSERT_EQ(set.cardinality(Trit(trit::True)), 17);
    ASSERT_EQ(set.cardinality(Trit(trit::False)), 1);
    ASSERT_EQ(set.cardinality(Trit()), 2);
    set.trim(5);
    ASSERT_EQ(set.to_string(), "01?11???????????????????????????");
    ASSERT_EQ(set.cardinality(Trit(trit::True)), 3);
    ASSERT_EQ(set.cardinality(Trit(trit::False)), 1);
    ASSERT_EQ(set.cardinality(Trit()), 1);
}


TEST (tritset_tests, resize_shrink) {
    Tritset set(40);
    set[2] = trit::True;
    ASSERT_EQ(set.to_string(), "??1?????????????????????????????????????????????");
    set.shrink();
    ASSERT_EQ(set.to_string(), "??1?????????????");
    ASSERT_EQ(set.capacity(), 16);
    set[20] = trit::True;
    set.shrink();
    ASSERT_EQ(set.capacity(), 32);
}


TEST (tritset_tests, resize_massive) {
    Tritset set(40, trit::True);
    set.resizeMassive(60);
    ASSERT_EQ(set.capacity(), 64);
    ASSERT_EQ(set.length(), 40);
    ASSERT_EQ (set.cardinality(Trit(trit::True)), 40);
}


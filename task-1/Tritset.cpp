#include "Tritset.h"

Tritset::Tritset() {
    array.resize(0);
    _last = -1;
}
Tritset::Tritset(const size_t size)
{
    size_t needed = size / consts::sizePerElement;
    needed += ((size % consts::sizePerElement) ? 1 : 0);
    array.resize(needed,fill_value(trit::Unknown));
    _last = -1;
}
Tritset::Tritset(const size_t size, const Trit new_value)
{
    _last = (int) size - 1;
    size_t needed = size / consts::sizePerElement;
    needed += ((size % consts::sizePerElement) ? 1 : 0);
    bool flag = (size % consts::sizePerElement) > 0;
    switch (new_value.value()) {
        case trit::False:
            array.resize(needed, fill_value(trit::False));
            break;
        case trit::True:
            array.resize(needed, fill_value(trit::True));
            break;
        case trit::Unknown:
            array.resize(needed, fill_value(trit::Unknown));
            _last = -1;
            break;
    }
    if (flag && new_value.value() != trit::Unknown)
        for (size_t i = size; i < needed * consts::sizePerElement; ++i)
            (*this)[i] = trit::Unknown;
}

unsigned Tritset::fill_value(consts::trit value)
{
    unsigned elem;
    switch (value)
    {
        case trit::False:
            elem = static_cast<unsigned>(trit::False);
            break;
        case trit::True:
            elem = static_cast<unsigned>(trit::True);
            break;
        case trit::Unknown:
            elem = static_cast<unsigned>(trit::Unknown);
            break;
    }
    unsigned res = unsigned(0);
    for (int i = 0; i < consts::sizePerElement; ++i)
        res |= elem << (i * 2);
    return res;
}
void Tritset::updateLastIndex()
{
    _last = -1;

    size_t len = array.size() * consts::sizePerElement;
    for (size_t i = 0; i < len; ++i) {
        if ((*this)[i].get_value() != trit::Unknown)
            _last = i;
    }
}

Tritset &Tritset::operator&=(const Tritset &other)
{
    size_t len = other.capacity();
    resizeMassive(len);
    for (int i = 0; i < len; ++i)
        (*this)[i] &= Trit(other[i]);
    for (size_t i = len; i <= this->capacity(); ++i)
        (*this)[i] &= Trit(trit::Unknown);
    updateLastIndex();
    return (*this);
}

Tritset &Tritset::operator|=(const Tritset &other)
{
    size_t len = other.length();
    resizeMassive(len);
    for (size_t i = 0; i < len; ++i)
        (*this)[i] |= other[i];
    for (size_t i = len; i <= _last; ++i)
        (*this)[i] |= Trit(trit::Unknown);
    updateLastIndex();
    return (*this);
}
Tritset Tritset::operator~() const {
    Tritset cp(*this);
    for (int i = 0; i <= _last; ++i)
        (cp[i]) = !(cp[i]);
    cp.updateLastIndex();
    return cp;
}

Trit Tritset::operator[](std::size_t position) const
{
    int value = array[position / consts::sizePerElement];
    value >>= 2 * (position % consts::sizePerElement);
    value &= static_cast<unsigned>(consts::mask);
    if (value == static_cast<unsigned>(trit ::False))
        return Trit(trit::False);
    if (value == static_cast<unsigned>(trit::True))
        return Trit(trit::True);
    return Trit(trit::Unknown);
}

Tritset::reference Tritset::operator[](std::size_t pos)
{
    return Tritset::reference(this, pos);
}

void Tritset::resizeMassive(const size_t new_size)
{
    size_t needed = new_size / consts::sizePerElement;
    needed += ((new_size % consts::sizePerElement) ? 1 : 0);
    if (needed > array.size())
        array.resize(needed,fill_value(trit::Unknown));
}


size_t Tritset::cardinality(Trit value) const
{
    size_t len = this->length();
    size_t counter = 0;
    for (int i = 0; i <len; ++i) {
        Trit t = (*this)[i];
        if (t == value)
            ++counter;
    }

        return counter;
}
void Tritset::trim(size_t lastIndex)
{
    size_t len = (*this).capacity();
    if(len>lastIndex)
    {
        for(size_t i = lastIndex; i<= len;++i)
        {
            (*this)[i] = trit::Unknown;
        }
    }
    _last = lastIndex - 1;
}

size_t Tritset::capacity() const {
    return array.size() * consts::sizePerElement;
}

size_t Tritset::length() const {
    return _last + 1;
}

size_t Tritset::size() const {
    return array.size();
}

size_t Tritset::shrink() {
    if (_last < 0) {
        array.resize(0);
        _last = -1;
        return 0;
    }
    size_t needed = _last / consts::sizePerElement;
    needed += ((_last % consts::sizePerElement) ? 1 : 0);
    array.resize(needed);
    for (size_t i = _last + 1; i < array.size() *  consts::sizePerElement; ++i)
        (*this)[i] = trit::Unknown;
    Tritset::updateLastIndex();
    return needed;
}
std::string Tritset::to_string() const {
    std::string str;
    size_t len = this->capacity();
    str.resize(len);
    for (int i = 0; i < len; ++i) {
        Trit t = (*this)[i];
        switch(t.value())
        {
            case trit::True:
                str[i] = '1';
                break;
            case trit::False:
                str[i] = '0';
                break;
            case trit::Unknown:
                str[i] = '?';
                break;

        }
    }
    return str;
}


Tritset operator&( const Tritset& x, const Tritset& y)
{
    Tritset result= Tritset(x);
    result &=y;
    return result;
}
Tritset operator|( const Tritset& x, const Tritset& y) {
    Tritset result = Tritset(x);
    result |= y;
    return result;
}
#include "Trit.h"

Trit::Trit() : _value(trit::Unknown) {}

Trit::Trit(trit value)
{
    _value = value;
}

Trit &Trit::operator&=(const Trit &other)
{
    if(_value == trit::False || other._value == trit::False)
        _value = trit::False;
    else if(_value == trit::Unknown || other._value == trit::Unknown)
        _value = trit::Unknown;
    return *this;
}

Trit &Trit::operator|=(const Trit &other)
{
    if (_value == trit::True || other._value == trit::True)
        _value = trit::True;
    else if (_value == trit::Unknown || other._value == trit::Unknown)
        _value = trit::Unknown;
    return *this;
}

Trit Trit::operator!() const {
    if (_value == trit::False)
        return Trit(trit::True);
    if (_value == trit::True)
        return Trit(trit::False);
    return Trit(trit::Unknown);
}

Trit &Trit::operator=(const Trit &other)
{
    _value = other._value;
    return (*this);
}

bool Trit::operator==(const Trit &other) const
{
    return _value == other._value;
}

bool Trit::operator!=(const Trit &other) const
{
    return _value != other._value;
}

trit Trit::value() const
{
    return _value;
}

Trit operator&( const Trit& x, const Trit& y)
{
    Trit result(x);
    result &= y;
    return result;
}
Trit operator|( const Trit& x, const Trit& y)
{
    Trit result(x);
    result |= y;
    return result;
}
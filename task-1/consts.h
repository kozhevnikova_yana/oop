#pragma once

namespace consts
{
    enum class trit{False = 1,Unknown = 0, True = 3};
    const unsigned int sizePerElement(sizeof(unsigned) * 8 / 2);
    const int mask = 0b11;
}
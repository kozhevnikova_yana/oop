#pragma once
#include "consts.h"

using consts::trit;
class Trit
{
private:
    consts::trit _value;
public:
    Trit();
    Trit(const trit value);
    Trit &operator&=(const Trit &other);
    Trit &operator|=(const Trit &other);
    Trit operator!() const;
    Trit &operator=(const Trit &other);
    bool operator==(const Trit &other) const;
    bool operator!=(const Trit &other) const;
    trit value() const;
};

Trit operator&( const Trit& x, const Trit& y);
Trit operator|( const Trit& x, const Trit& y);
#pragma once

#include <stdio.h>
#include <iostream>
#include<vector>
#include <math.h>
#include <map>
#include "consts.h"
#include "Trit.h"
using  consts::trit;
class Tritset {
private:
    unsigned fill_value(trit value);
    std::vector<unsigned> array;
    void updateLastIndex();
    long long int _last;
public:
    Tritset();
    Tritset(const size_t);
    Tritset(const size_t size, const Trit new_value);
    class reference
    {
    public:
        Tritset *_set;
        size_t _index;
    public:
        reference(Tritset *set, size_t index);

        reference &operator=(const Trit &other);
        reference &operator=(const reference &other);

        bool operator==(const reference &other);
        bool operator==(const Trit &other);
        bool operator!=(const reference &other);
        Trit operator!() const;

        reference &operator&=(const Trit &other);
        reference &operator|=(const Trit &other);

        Trit get_value() const;
        void set_value(const Trit &value);
    };

    Tritset &operator&=(const Tritset &other);
    Tritset &operator|=(const Tritset &other);
    Tritset operator~() const;

    Trit operator[]( std::size_t pos) const;
    reference operator[]( std::size_t pos);
    void resizeMassive(const size_t new_size);
    size_t cardinality(Trit value) const;
    void trim(size_t lastIndex);
    size_t capacity() const;
    size_t length() const;
    size_t size() const;
    size_t shrink();
    std::string to_string() const;
};

Tritset operator&( const Tritset& x, const Tritset& y);
Tritset operator|( const Tritset& x, const Tritset& y);

#include "Tritset.h"
Tritset::reference::reference(Tritset *set, size_t index) : _set(set), _index(index) {}
Tritset::reference &Tritset::reference::operator=(const Trit &other) 
{
    set_value(other);
    _set->updateLastIndex();
    return (*this);
}
Tritset::reference &Tritset::reference::operator=(const Tritset::reference &other) 
{
    (*this) = other.get_value();
    _set->updateLastIndex();
    return (*this);
}

bool Tritset::reference::operator==(const Tritset::reference &other)
{
    return other._index == _index;
}
bool Tritset::reference::operator==(const Trit &other) 
{
    return this->get_value() == other.value();
}
bool Tritset::reference::operator!=(const Tritset::reference &other)
{
    return other._index != _index;
}

Tritset::reference &Tritset::reference::operator&=(const Trit &other) 
{
    set_value(get_value()&other);
    return (*this);
}

Tritset::reference &Tritset::reference::operator|=(const Trit &other) 
{
    set_value(get_value()|other);
    return (*this);
}

Trit Tritset::reference::get_value() const 
{
    if (_index >= _set->capacity())
        return Trit(trit::Unknown);
    int value = _set->array[_index / consts::sizePerElement];
    value >>= 2 * (_index % consts::sizePerElement);
    value &= 0b11;
    if (value == 0)
        return Trit(trit::Unknown);
    if (value == 0b11)
        return Trit(trit::True);
    return Trit(trit::False);
}
void Tritset::reference::set_value(const Trit &value)
{
    if (_index >= _set->capacity())
    {
        if (value == trit::Unknown)
            return;
        _set->resizeMassive(_index + 1);
    }
    size_t id = _index / consts::sizePerElement;
    size_t shift = 2 * (_index % consts::sizePerElement);

    _set->array[id] &= ~(static_cast<unsigned>(consts::mask) << shift);
    if (value == trit::True)
        _set->array[id] |= static_cast<unsigned>(trit ::True) << shift;
    else if (value == trit::Unknown)
        _set->array[id] |= static_cast<unsigned>(trit ::Unknown) << shift;
    else
        _set->array[id] |= static_cast<unsigned>(trit::False) << shift;

    if ( _index > _set->_last && value != trit::Unknown)
        _set->_last = static_cast<int>(_index);
}

Trit Tritset::reference::operator!() const
{
    Trit result = !get_value();
    return result;
}

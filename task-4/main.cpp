#include <iostream>
#include <string>

#include "CsvParser.h"
#include "printTuple.h"
int main()
{
    std::ifstream file("test.csv", std::ios::binary);
    if (!file.is_open())
        throw std::runtime_error("Could not open file!\n");
    try
    {
        CSVParser<int, std::string > parser(file, 0 ,';','"','\n');
        for (auto rs : parser)
        {
            std::cout << rs << std::endl;
        }
    }
    catch(exceptions::equalDifDelimiters &e)
    {
        std::cerr<<e.what();
    }
    return 0;
}


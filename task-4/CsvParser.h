#pragma once
#include <string>
#include <fstream>
#include <tuple>
#include <vector>
#include <iostream>
#include <iterator>
#include <string.h>
#include <sstream>
#include "exceptions.h"
#include "consts.h"

template  <typename T1, typename... Args>
class CSVParser
{
    std::vector <std::tuple <T1,Args...>> v;

    int header;
    char sep;
    char screening;
    char delimiterLines;
    int counterScreening = 0;
    int indexRow = 0, indexCol = 0;
public:
    explicit CSVParser(std::ifstream &file, int header = 0,  char sep = ',', char screening = '"', char delimiterLines = '\n'):
            header(header), sep(sep), screening(screening), delimiterLines(delimiterLines)
    {
        if(sep == screening)
            throw exceptions::equalDifDelimiters();
        std::string headerns = "";
        while (header > 0)
        {
            std::getline(file, headerns, delimiterLines);
            --header;
        }
        while (has_dataset(file))
        {
            try{
                auto t = readline(file);
                v.push_back(t);
            }
            catch(exceptions::errorParser &e)
            {
                std::cerr<<e.what()<<indexRow<<","<<indexCol;
                break;
            }
        }
    }
    bool has_dataset(std::ifstream &file)
    {
        char check;
        file>> check;
        bool endOfFile = !(file.eof());
        file.seekg(-1, std::ios_base::cur);
        return endOfFile;
    }

    std::tuple<T1, Args...> readline(std::ifstream &file)
    {
        std::tuple<T1, Args...> row;
        std::string buf = raw_readline(file);
        ++indexRow;
        indexCol = 0;
        std::stringstream s(buf);

       filltuple(row, s);
        return row;
    }

    std::string raw_readline(std::ifstream &file)
    {
        file.clear();
        std::string row;
        std::getline(file, row, delimiterLines);
        return row;
    }

    template<std::size_t I = 0, typename... Tp>
    inline typename std::enable_if<I == sizeof...(Tp), void>::type
    filltuple(std::tuple<Tp...>&, std::stringstream&)
    { }

    template<std::size_t I = 0, typename... Tp>
    inline typename std::enable_if<(I < sizeof...(Tp)), void>::type
    filltuple(std::tuple<Tp...>& t, std::stringstream& s)
    {
        std::string buf;
        std::string col;
        std::getline(s, buf, sep);
        ++indexCol;
        for( int i = 0; i<buf.length();++i)
        {
            if(buf.at(i) == screening && ( i== consts::IndexBegining  || buf[i-1] != screening))
            {
                ++counterScreening;
                continue;
            }
            if(counterScreening % 2 == 0)
            {
                col += buf[i];
            }
        }
        if(counterScreening %2 != 0)
            throw exceptions::errorParser();
        std::stringstream rowstream(col);
        rowstream >> std::get<I>(t);
        filltuple<I+1, Tp...>(t, s);
    }

    auto begin()
    {
        return v.begin();
    }
    auto end()
    {
        return v.end();
    }
};

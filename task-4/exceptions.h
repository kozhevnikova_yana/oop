#include <exception>
namespace exceptions
{
    class errorParser: public std::exception
    {
    public:
        const char *what() const noexcept;
    };
    class equalDifDelimiters: public std::exception
    {
    public:
        const char *what() const noexcept;
    };
}

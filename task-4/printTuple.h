#pragma once

#include <tuple>
#include <iostream>

template<class Ch, class Tr, size_t N, typename... Args>
struct TuplePrinter {
    static void print(std::basic_ostream<Ch,Tr> & out, const std::tuple<Args...> & t)
    {
        TuplePrinter<Ch, Tr, N-1, Args...>::print(out, t);
        out << ", " << std::get<N-1>(t);
    }
};

template<class Ch, class Tr, typename... Args>
struct TuplePrinter<Ch, Tr, 1, Args...> {
    static void print(std::basic_ostream<Ch,Tr> & out, const std::tuple<Args...> & t)
    {
        out << std::get<0>(t);
    }
};
template<class Ch, class Tr, typename... Args>
std::ostream & operator<<(std::basic_ostream<Ch,Tr> & out, const std::tuple<Args...> & t)
{
    out << "(";
    TuplePrinter<Ch, Tr, sizeof...(Args), Args...>::print(out, t);
    return out << ")";
}
#include <iostream>
#include <time.h>
#include "Game/GameConfiguration.h"
#include "Game/Game.h"
#include "Game/ParserConditions.h"


int main(int argc, char* argv[])
{
    GameConfiguration config;
    ParserConditions parser;
    int result_parser = parser.makeNewConfiguration(argc,argv, config);
    switch(result_parser)
    {
        case consts::PARSE_ERROR:
            return 1;
        case consts::PARSE_HELP:
            return 0;
    };
    srand(time(NULL));
    Game g(config);
    g.runGame();
    return 0;
}
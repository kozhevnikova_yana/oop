#pragma once

#include "../Game/consts.h"
class Coordinate
{
private:
    int x;
    int y;
public:
    Coordinate(){};
    Coordinate(int newX, int newY);
    int getX() const;
    int getY() const;
    void setX(int newX);

    void setY(int newY);
    ~Coordinate() {};
};


const bool operator==(const class Coordinate& f1, const class Coordinate& f2);
const bool operator<(const class Coordinate& f1, const class Coordinate& f2);
const bool operator==(const class Coordinate& f1, const class std::pair<Coordinate, consts::StatesShot>& f2);

#pragma once
#include <iostream>
#include <list>
#include <time.h>
#include <windows.h>
#include <conio.h>
#include "../Game/consts.h"
#include"Ship.h"
#include "Cell.h"
using std::list;

class Field
{
private:
    string titles[consts::SIZEFIELD] = { " A ", " B ", " C ", " D ", " E ", " F ", " G ", " H ", " I ", " J " };
    Cell mapField[consts::SIZEFIELD][consts::SIZEFIELD];
    int countShips = 0;
    int counterEachTypeShip[consts::HOW_MUCH_SHIPS_EACH_TYPE] = {0};
    Ship ships[consts::SIZEARMY];
    void drawHeader();
    bool validShot(int beginx, int beginy, int endx, int endy);
public:
    Field();

    bool addShip(int x_begin, int y_begin, int x_end, int y_end);
    int getCntShip() const;
    consts::StatesShot checkGuess(Coordinate guess);
    void drawForMe();
    void drawForEnemy();
    ~Field();
};


#include "Cell.h"
Cell::Cell() {}
bool Cell::isFree() const
{
    return _isFree;
}

void Cell::notFree()
{
    _isFree = false;
}


bool Cell::isHit() const
{
    return _hit;
}

void Cell::hasHit()
{
    _hit = true;
}

Cell::~Cell() {}
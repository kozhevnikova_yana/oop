#include "Field.h"
Field::Field()
{
}
bool Field::validShot(int beginx, int beginy, int endx, int endy)
{
    if( (beginx < 0 || beginx >= consts::SIZEFIELD) || (beginy < 0 || beginy >= consts::SIZEFIELD)
        || (endx < 0 || endx >= consts::SIZEFIELD) || (endy < 0 || endy >= consts::SIZEFIELD))
        return false;

    for (auto i = beginx - 1; i <= endx + 1; ++i)
    {
        for (auto j = beginy - 1; j <= endy + 1; ++j)
        {
            if (i >= 0 && i < consts::SIZEARMY && j >= 0 && j < consts::SIZEARMY)
            {
                if (!mapField[i][j].isFree())
                {
                    return false;
                }
            }
        }
    }
    return true;
}
bool Field::addShip(int beginx, int beginy, int endx, int endy)
{
    if (beginx > endx)
    {
        std::swap(beginx, endx);
    }
    if (beginy > endy)
    {
        std::swap(beginy, endy);
    }

    if(!validShot(beginx, beginy, endx, endy))
        return false;

    int index = abs(beginx - endx) + abs(beginy - endy);
    int size = index + 1;
    if (counterEachTypeShip[index] > consts::HOW_MUCH_SHIPS_EACH_TYPE - size)
        return false;

    for (auto i = beginx; i <= endx; ++i)
    {
        for (auto j = beginy; j <= endy; ++j)
        {
            mapField[i][j].notFree();
            Coordinate c(i,j);
            ships[countShips].addLocation(c);
        }
    }
    ++countShips;
    ++counterEachTypeShip[index];
    return true;
}

consts::StatesShot Field::checkGuess(Coordinate guess)
{
    mapField[guess.getX()][guess.getY()].hasHit();
    for(int i = 0; i < consts::SIZEARMY; ++i)
    {
        if (ships[i].fireUpon(guess))
        {
            if (ships[i].sunk())
            {
                --countShips;
                std::cout << consts::STRING_IF_KILLED << std::endl;
                return consts::StatesShot::KILLED;
            }
            else
            {
                std::cout << consts::STRING_IF_INJURED << std::endl;
                return consts::StatesShot::INJURED;
            }
        }
    }
    std::cout << consts::STRING_IF_EMPTY << std::endl;
    return consts::StatesShot::EMPTY;
}

int Field::getCntShip() const
{
    return countShips;
}

void Field::drawHeader()
{
    std::cout << consts::THREE_SPACES << consts::VERTICAL_BORDER;
    for (int i = 0; i < consts::SIZEFIELD - 1; ++i)
    {
        std::cout <<consts::lEFT_SPACE_FOR_NUMBERS<< i + 1 <<consts::ONE_SPACE<<consts::VERTICAL_BORDER;
    }
    std::cout << consts::TWO_SPACES<<consts::SIZEFIELD << consts::VERTICAL_BORDER;
    std::cout << std::endl<< consts::HORIZONTAL_BORDER << std::endl;
}

void Field::drawForMe()
{
    drawHeader();
    for (int i = 0; i < consts::SIZEFIELD; ++i)
    {
        std::cout << titles[i] << consts::VERTICAL_BORDER;
        for (int j = 0; j < consts::SIZEFIELD; ++j)
        {

            if (!mapField[i][j].isFree() && mapField[i][j].isHit())
            {
                std::cout << consts::HIT;
            }
            else if (mapField[i][j].isFree() && mapField[i][j].isHit())
            {
                std::cout << consts::MISS;
            }
            else if (!mapField[i][j].isFree())
                std::cout << consts::SHIP;
            else
                std::cout << consts::SPACE_IN_CELL;
            std::cout <<consts::VERTICAL_BORDER;

        }
        std::cout << std::endl << consts::HORIZONTAL_BORDER << std::endl;
    }
}

void Field::drawForEnemy()
{
    drawHeader();
    for (int i = 0; i < consts::SIZEFIELD; ++i)
    {
        std::cout << titles[i] <<  consts::VERTICAL_BORDER;
        for (int j = 0; j < consts::SIZEFIELD; ++j)
        {
            if (!mapField[i][j].isFree() && mapField[i][j].isHit())
            {
                std::cout << consts::HIT;
            }
            else if (mapField[i][j].isFree() && mapField[i][j].isHit())
            {
                std::cout << consts::MISS;
            }
            else
                std::cout << consts::SPACE_IN_CELL;
            std::cout <<  consts::VERTICAL_BORDER;

        }
        std::cout << std::endl << consts::HORIZONTAL_BORDER << std::endl;
    }
}
Field::~Field()
{
}

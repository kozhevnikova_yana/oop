#include "Ship.h"

Ship::Ship()
{
}

bool Ship::fireUpon(Coordinate c)
{
    auto it = find(location.begin(), location.end(), c);
    if (it!= location.end())
    {
         location.remove(c);

        if (location.empty())
            isSunk = true;

        return true;
    }
    else
    {
        return false;
    }
}

void Ship::addLocation(Coordinate &c)
{
    location.push_back(c);
}
bool Ship::sunk()
{
    return isSunk;
}


Ship::~Ship()
{
}
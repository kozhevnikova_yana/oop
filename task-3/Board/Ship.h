#pragma once

#include <algorithm>
#include <list>
#include "../Game/consts.h"
#include "Cell.h"
#include "Coordinate.h"
using std::list;
class Ship
{
private:
    list<Coordinate> location;
    bool isSunk = false;
public:
    Ship();
    void addLocation(Coordinate &c);
    bool fireUpon(Coordinate c);
    bool sunk();
    ~Ship();
};


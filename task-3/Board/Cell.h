#pragma once
class Cell
{
private:
    bool _isFree = true;
    bool _hit = false;
public:
    Cell();
    bool isFree() const;
    bool isHit() const;
    void notFree();
    void hasHit();
    ~Cell();
};
#include "Coordinate.h"


Coordinate::Coordinate(int newX, int newY)
{
    x = newX;
    y = newY;
}

int Coordinate::getX() const
{
    return x;
}
int Coordinate::getY() const
{
    return y;
}

void Coordinate::setX(int newX)
{
    x = newX;
}

void Coordinate::setY(int newY)
{
    y = newY;
}

const bool operator==(const Coordinate& f1, const  Coordinate& f2)
{
    return (f1.getX() == f2.getX() && (f1.getY() == f2.getY()));
}
const bool operator<(const class Coordinate& f1, const class Coordinate& f2)
{
    return ((f1.getX() == f2.getX()) && (f1.getY() < f2.getY())) || ((f1.getX() < f2.getX()) && (f1.getY() == f2.getY()));
}
const bool operator==(const Coordinate& f1, const std::pair<Coordinate, consts::StatesShot>& f2)
{

    return  f1 == f2.first;
}
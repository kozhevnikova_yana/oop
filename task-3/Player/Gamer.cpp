#include "Gamer.h"

Gamer::Gamer()
{
}

Gamer::Gamer(string name)
{
    this->name = name;
}
string Gamer::getnameGamer()
{
    return this->name;
}

bool Gamer::validShot(Coordinate c)
{
    bool valid = true;
    for(auto i: historyShots)
    {
        if(i.first == c)
        {
            valid = false;
            break;
        }
    }

    if((c.getX() < 0 || c.getY() < 0) || (c.getX() >= consts::SIZEFIELD || c.getY()  >= consts::SIZEFIELD ))
        valid = false;
    return valid;
}

void Gamer::addShot(Coordinate c, consts::StatesShot state)
{
    historyShots.emplace_back(std::make_pair(c,state));
}
Field* Gamer::getField()
{
    return &field;
}
int Gamer::getCntShip()
{
    return field.getCntShip();
}

std::pair<Coordinate, consts::StatesShot> Gamer::getLastShot()
{
    return historyShots.back();
}


Gamer::~Gamer()
{
}

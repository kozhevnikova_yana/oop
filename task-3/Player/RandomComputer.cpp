#include "RandomComputer.h"

void RandomComputer::placeShips()
{
    while (getField()->getCntShip()<consts::SIZEARMY)
    {
        for(int size = consts::MAXLENGTHSHIP; size >= 0; --size)
        {
            int cnt = 4 - size;
            while(cnt > 0)
            {
                int x = rand() % consts::SIZEFIELD;
                int y = rand() % consts::SIZEFIELD;
                int isHorizontal = rand() % 2;
                int condition1 = x + size* isHorizontal;
                int condition2 = y + size * ((isHorizontal + 1) % 2);
                if( condition1 >= 0  && condition1 < consts::SIZEFIELD && condition2 >= 0 && condition2 < consts::SIZEFIELD)
                {
                    if(getField()->addShip(x, y, condition1 , condition2))
                        --cnt;

                }
            }
        }
    }
}

Coordinate RandomComputer::fire()
{
    bool hit = false;
    Coordinate guess;
    while(!hit)
    {
        guess.setX(rand() % consts::SIZEFIELD);
        guess.setY(rand() % consts::SIZEFIELD);
        hit = validShot(guess);
    }
    std::cout << static_cast<char>((guess.getX()) + static_cast<int>(consts::FIRST_LETTER_IN_COORDINATE)) << guess.getY() + 1 << std::endl;
    return guess;
}
RandomComputer::~RandomComputer()
{
}
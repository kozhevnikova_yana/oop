#pragma once

#include <algorithm>
#include "../Board/Field.h"
#include <stack>
class Gamer
{
private:
    Field field;
    string name;
    std::list<std::pair<Coordinate, consts::StatesShot>> historyShots;

public:
    Gamer();
    Gamer(string name);
    virtual Coordinate fire() = 0;
    virtual void placeShips() = 0;
    int getCntShip();
    Field* getField();

    std::pair<Coordinate, consts::StatesShot> getLastShot();
    bool validShot(Coordinate c);
    void addShot(Coordinate c, consts::StatesShot state);
    string getnameGamer();
    virtual ~Gamer();
};


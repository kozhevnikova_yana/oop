#pragma once
#include "Gamer.h"
#include <stack>
using std::stack;
using std::deque;
using std::list;
class CleverComputer : public Gamer
{
private:
    std::list<Coordinate> previousLuckyShots;

    void firstStep(Coordinate &guess);
    void secondStep( Coordinate &guess);
    void nextSteps(Coordinate &guess);

public:
    CleverComputer(const string name): Gamer(name){};
    virtual Coordinate fire() override;
    virtual void placeShips() override;
    ~CleverComputer();
};


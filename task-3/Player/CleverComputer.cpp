#include "CleverComputer.h"

void CleverComputer::placeShips()
{
    while (getField()->getCntShip()<consts::SIZEARMY)
    {
        for(int size = consts::MAXLENGTHSHIP; size >= 0; --size)
        {
            int cnt = consts::HOW_MUCH_SHIPS_EACH_TYPE - size;
            while(cnt > 0)
            {
                int x = rand() % consts::SIZEFIELD;
                int y = rand() % consts::SIZEFIELD;
                int isHorizontal = rand() % consts::HORIZ_OR_VERTIC;
                int condition1 = x + size* isHorizontal;
                int condition2 = y + size * ((isHorizontal + 1) % consts::HORIZ_OR_VERTIC);
                if( condition1 >= 0  && condition1 < consts::SIZEFIELD && condition2 >= 0 && condition2 < consts::SIZEFIELD)
                {
                    if(getField()->addShip(x, y, condition1 , condition2))
                        --cnt;

                }
            }
        }
    }
}

Coordinate CleverComputer::fire()
{
    std::pair<Coordinate, consts::StatesShot> prShot = getLastShot();
    switch (prShot.second)
    {
        case consts::KILLED:
            while (!previousLuckyShots.empty())
                previousLuckyShots.pop_front();
            break;
        case consts::INJURED:
                previousLuckyShots.push_front(prShot.first);
                previousLuckyShots.sort();
            break;
        case consts::EMPTY:
            break;

    };
    Coordinate guess;
    if(previousLuckyShots.empty())
        firstStep(guess);
    else  if(previousLuckyShots.size() == 1)
        secondStep(guess);
    else
        nextSteps(guess);
    std::cout << static_cast<char>((guess.getX()) + static_cast<int>(consts::FIRST_LETTER_IN_COORDINATE)) << guess.getY() + 1 << std::endl;
    return guess;
}

void CleverComputer::firstStep(Coordinate &guess)
{
    bool hit = false;
    while(!hit)
    {
        guess.setX(rand() % consts::SIZEFIELD);
        guess.setY(rand() % consts::SIZEFIELD);
        hit = validShot(guess);
    }
}

void CleverComputer::secondStep(Coordinate &guess)
{
    Coordinate lastShot = previousLuckyShots.front();
    bool hit = false;
    while(!hit)
    {
        int newDir = rand() % consts::NUM_DIR;

        guess.setX(lastShot.getX());
        guess.setY(lastShot.getY());

        int x = guess.getX();
        int y = guess.getY();
        switch (newDir)
        {
            case consts::LEFT:
                if ((x - 1) >= 0)
                    guess.setX(--x);
                break;
            case consts::RIGHT:
                if ((x + 1) < consts::SIZEFIELD)
                    guess.setX(++x);
                break;
            case consts::UP:
                if (y >= 0)
                    guess.setY(--y);
                break;
            case consts::DOWN:
                if ((y + 1) < consts::SIZEFIELD)
                    guess.setY(++y);
                break;
        };
        hit = validShot(guess);
    }
}
void CleverComputer::nextSteps(Coordinate &guess)
{
    Coordinate lastShot = previousLuckyShots.front();
    Coordinate firstShot = previousLuckyShots.back();

    if (lastShot.getX() == firstShot.getX()) // he id horizontal
    {
        guess.setX(lastShot.getX());
            if (validShot(Coordinate(lastShot.getX(), lastShot.getY() - 1)))
                guess.setY(lastShot.getY() - 1);
            else
                guess.setY(firstShot.getY() + 1);
    }
    else // he is vertical
    {
        guess.setY(lastShot.getY());
        if (validShot(Coordinate(lastShot.getX() - 1, lastShot.getY())))
                guess.setX(lastShot.getX() - 1);
        else
                guess.setX(firstShot.getX() + 1);
    }
}

CleverComputer::~CleverComputer()
{
}
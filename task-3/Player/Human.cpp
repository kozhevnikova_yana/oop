#include "Human.h"
#include <windows.h>

namespace
{

    const string WRONG_SHOT = "You can't do it";
    const string GET_COORDINATES =  "Coordinate ship:";
    const string STRING_BEFOR_NAME =  "Your ships(";
    const string STRING_AFTER_NAME = "):";
}
void Human::placeShips()
{
    while (getField()->getCntShip() != consts::SIZEARMY)
    {
        std::cout << STRING_BEFOR_NAME<< getnameGamer() << STRING_AFTER_NAME << std::endl;
        getField()->drawForMe();
        std::cout << GET_COORDINATES << std::endl;
        char ax, bx;
        int ay, by;
        std::cin >> ax >> ay >> bx >> by;
        ax -= consts::FIRST_LETTER_IN_COORDINATE;
        bx -= consts::FIRST_LETTER_IN_COORDINATE;
        if (!getField()->addShip(ax, --ay, bx, --by))
            std::cout << WRONG_SHOT << std::endl;
        system("pause");
        system("cls");
    }
}

Coordinate Human::fire()
{
    bool hit = false;
    Coordinate guess;
    while (!hit)
    {
        char x; int y;
        std::cin >> x >> y;
        x -= consts::FIRST_LETTER_IN_COORDINATE;
        --y;
        guess.setX(x);
        guess.setY(y);
        hit = validShot(guess);
    }
    return guess;
}
Human::~Human()
{
}
#pragma once
#include "Gamer.h"
class RandomComputer : public Gamer
{
private:
public:
    RandomComputer(const string name): Gamer(name){};
    Coordinate fire() override;
    void placeShips() override;
    ~RandomComputer();
};

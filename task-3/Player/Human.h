#pragma once

#include "Gamer.h"
#include <string>
class Human : public Gamer
{
public:
    Human(const string name): Gamer(name){};
    virtual Coordinate fire() override;
    virtual void placeShips() override;
    ~Human();
};


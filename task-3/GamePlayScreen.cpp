#include "GamePlayScreen.h"

GameplayScreen::GameplayScreen()
{
}
void GameplayScreen::init(GameConfiguration &config)
{
    if(config.nameFirstGamer == consts::HUMAN)
            gamer1 = new Human(consts::NAMEFIRSTPLAYER);
    else if(config.nameFirstGamer == consts::RANDOMCOMPUTER)
        gamer1 = new RandomComputer(consts::NAMEFIRSTPLAYER);
    else if(config.nameFirstGamer == consts::CLEVERCOMPUTER)
        gamer1 = new CleverComputer(consts::NAMEFIRSTPLAYER);

    if(config.nameSecondGamer == consts::HUMAN)
        gamer2 = new Human(consts::NAMESECONDPLAYER);
    else if(config.nameSecondGamer == consts::RANDOMCOMPUTER)
        gamer2 = new RandomComputer(consts::NAMESECONDPLAYER);
    else if(config.nameSecondGamer == consts::CLEVERCOMPUTER)
        gamer2 = new CleverComputer(consts::NAMESECONDPLAYER);
}

void GameplayScreen::getShips()
{
    gamer1->placeShips();
    gamer2->placeShips();
}
void GameplayScreen::stepOneGamer(Gamer *first, Gamer *second)
{
    bool hit = true;
    while (hit  && second->getCntShip() > 0)
    {
        system("cls");
        std::cout <<"Gamer " <<first->getnameGamer()<<std::endl;
        std::cout<<"Field of Enemy\n";
        second->getField()->drawForEnemy();
        std::cout <<first->getnameGamer()<<":" ;
        Coordinate guess = first->fire();
        consts::StatesShot state = second->getField()->checkGuess(guess);
        if(state == consts::EMPTY)
            hit = false;
        first->addShot(guess,state);
        system("pause");


        system("cls");
        std::cout<<"Field "<<first->getnameGamer()<<std::endl;
        first->getField()->drawForMe();
        std::cout<<"Field "<<second->getnameGamer()<<std::endl;
        second->getField()->drawForEnemy();
        system("pause");
    }
}

bool GameplayScreen::play()
{
    bool nowA = true;
    while (gamer1->getCntShip() > 0 && gamer2->getCntShip() > 0)
    {
        if(nowA)
        {
            stepOneGamer(gamer1, gamer2);
            nowA = false;
        }
        else
        {
            stepOneGamer(gamer2,gamer1);
            nowA = true;
        }
    }
    if (gamer1->getCntShip() > 0)
    {
        std::cout << "A is winner\n" << std::endl;
        return true;
    }
    else
    {
        std::cout << "B is winner\n" << std::endl;
        return false;
    }
}


GameplayScreen::~GameplayScreen()
{

}

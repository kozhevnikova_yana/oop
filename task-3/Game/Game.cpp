#include "Game.h"

Game::Game(GameConfiguration &config)
{
    cntGames = config.cntGames;
    screen.init(config);
}


void Game::runGame()
{
    for(int i = 0; i < cntGames; ++i)
    {
        screen.getShips();
        bool winFirst = screen.play();
        if(winFirst)
            ++winsA;
        else
            ++winsB;
    }

    if(winsA > winsB)
        std::cout<<consts::WIN_SPEACH1<<std::endl;
    else if(winsA < winsB)
        std::cout<<consts::WIN_SPEACH2<<std::endl;
    else
        std::cout<<consts::DEAD_HIT<<std::endl;
}
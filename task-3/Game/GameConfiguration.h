#pragma once

#include <string>
#include "consts.h"
using std::string;
struct GameConfiguration
{
    int cntGames = 1;
    string nameFirstGamer = consts::CLEVERCOMPUTER;
    string nameSecondGamer = consts::CLEVERCOMPUTER;
};

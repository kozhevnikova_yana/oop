#pragma once

#include <string>
#include<iostream>

#include "GameConfiguration.h"
#include "consts.h"
#include "../Player/Gamer.h"
#include "GamePlayScreen.h"
class Game
{
private:
   GameplayScreen screen;
   int cntGames = 0;
   int winsA = 0, winsB = 0;
public:
    Game(GameConfiguration &config);
    void  runGame();
};

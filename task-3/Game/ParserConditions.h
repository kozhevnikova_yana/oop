#pragma once

#include <iostream>
#include "consts.h"
#include "optionparser.h"
#include "GameConfiguration.h"
class ParserConditions
{
public:
    int makeNewConfiguration(int argc, char* argv[], GameConfiguration &config);
};
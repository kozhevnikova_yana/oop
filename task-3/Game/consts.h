#pragma once

#include <string>
#include <vector>
#include "optionparser.h"
using std::string;
using std::vector;

namespace consts
{
    enum StatesShot{ KILLED, INJURED, EMPTY};
    const int SIZEARMY = 10;
    const int SIZEFIELD = 10;
    const int HOW_MUCH_SHIPS_EACH_TYPE = 4;
    const int NUM_DIR = 4;
    const int MAXLENGTHSHIP = 3;

    const string NAMEFIRSTPLAYER = "A";
    const string NAMESECONDPLAYER = "B";
    const string MISS = "  O ";
    const string HIT = "  X ";
    const string SHIP = "  * ";
    const string VERTICAL_BORDER = "|";
    const string HORIZONTAL_BORDER = "------------------------------------------------------";
    const string SPACE_IN_CELL = "    ";
    const string ONE_SPACE = " ";
    const string TWO_SPACES = "  ";
    const string THREE_SPACES = "   ";
    const string lEFT_SPACE_FOR_NUMBERS = "  ";

    const string RATIO_SIGN = ":";
    const char FIRST_LETTER_IN_COORDINATE = 'a';
    const string FIELD = " FIELD ";
    const string HUMAN = "ConsoleGamer";
    const string CLEVERCOMPUTER = "OptimalGamer";
    const string RANDOMCOMPUTER = "RandomGamer";

    const int LEFT = 0;
    const int RIGHT = 1;
    const int UP = 2;
    const int DOWN = 3;

    const int PARSE_ERROR = 1;
    const int PARSE_HELP = 0;
    const int PARSE_CONTINUE = -1;
    const int HORIZ_OR_VERTIC = 2;
    const string STRING_IF_EMPTY = "Empty";
    const string STRING_IF_KILLED = "Killed";
    const string STRING_IF_INJURED = "Injured";
    const string WIN_SPEACH1 = "First winner of all game";
    const string WIN_SPEACH2 = "Second winner of all game";
    const string DEAD_HIT = "dead hit";
    const string WIN_LOCAL = " is winner";
}

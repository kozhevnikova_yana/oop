#include "ParserConditions.h"
struct Arg : public option::Arg
{
};
enum  optionIndex { HELP, COUNT, FIRST, SECOND};
const option::Descriptor usage[] = {
        { HELP,    0,"h", "help",    Arg::None,""},
        { COUNT,0,"c","count",Arg::Optional,""},
        { FIRST,0,"f","first",Arg::Optional,""},
        { SECOND,0,"s","second",Arg::Optional,""},
        {0, 0, 0, 0}};
int ParserConditions::makeNewConfiguration(int argc, char* argv[], GameConfiguration &config)
{
    argc -= (argc > 0); argv += (argc > 0); // skip program name argv[0] if present
    option::Stats stats(usage, argc, argv);
    option::Option* options = (option::Option*)calloc(stats.options_max, sizeof(option::Option));
    option::Option* buffer = (option::Option*)calloc(stats.buffer_max, sizeof(option::Option));
    option::Parser parse(usage, argc, argv, options, buffer);

    if (parse.error())
        return consts::PARSE_ERROR;

    if (options[HELP])
    {
        std::cout << "It's faight of ships";
        return consts::PARSE_HELP;
    }

    for (int i = 0; i < parse.optionsCount(); ++i)
    {
        option::Option& opt = buffer[i];
        switch (opt.index())
        {
            case COUNT:
                config.cntGames = std::stoi((std::string)(opt.arg));
                break;
            case FIRST:
                config.nameFirstGamer = (std::string)(opt.arg);
            case SECOND:
                config.nameSecondGamer = (std::string)(opt.arg);
        }
    }
    return  consts::PARSE_CONTINUE;
}
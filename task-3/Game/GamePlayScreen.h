#pragma once

#include <string>
#include <memory>
#include <iostream>
#include "consts.h"
#include "GameConfiguration.h"
#include "../Player/Gamer.h"
#include "../Player/Human.h"
#include "../Player/RandomComputer.h"
#include "../Player/CleverComputer.h"
#include "../Player/Human.h"

class GameplayScreen
{
private:
    Gamer *gamer1;
    Gamer *gamer2;

    void stepGamer(Gamer *first, Gamer *second);
    void update(string nameFirst);
public:
    GameplayScreen();
    void init(GameConfiguration &config);
    void getShips();
    bool play();
    ~GameplayScreen();
};
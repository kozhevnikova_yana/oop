#include "GamePlayScreen.h"

GameplayScreen::GameplayScreen()
{
}
void GameplayScreen::init(GameConfiguration &config)
{
    if(config.nameFirstGamer == consts::HUMAN)
            gamer1 = new Human(consts::NAMEFIRSTPLAYER);
    else if(config.nameFirstGamer == consts::RANDOMCOMPUTER)
        gamer1 = new RandomComputer(consts::NAMEFIRSTPLAYER);
    else if(config.nameFirstGamer == consts::CLEVERCOMPUTER)
        gamer1 = new CleverComputer(consts::NAMEFIRSTPLAYER);

    if(config.nameSecondGamer == consts::HUMAN)
        gamer2 = new Human(consts::NAMESECONDPLAYER);
    else if(config.nameSecondGamer == consts::RANDOMCOMPUTER)
        gamer2 = new RandomComputer(consts::NAMESECONDPLAYER);
    else if(config.nameSecondGamer == consts::CLEVERCOMPUTER)
        gamer2 = new CleverComputer(consts::NAMESECONDPLAYER);
}

void GameplayScreen::getShips()
{
    gamer1->placeShips();
    gamer2->placeShips();
}

void GameplayScreen::update(string nameFirst)
{
    if(nameFirst == gamer1->getnameGamer())
    {
        system("cls");
        std::cout<<consts::FIELD<<gamer1->getnameGamer()<<std::endl;
        gamer1->getField()->drawForMe();
        std::cout<<consts::FIELD<<gamer2->getnameGamer()<<std::endl;
        gamer2->getField()->drawForEnemy();
    } else
    {
        system("cls");
        std::cout<<consts::FIELD<<gamer1->getnameGamer()<<std::endl;
        gamer1->getField()->drawForEnemy();
        std::cout<<consts::FIELD<<gamer2->getnameGamer()<<std::endl;
        gamer2->getField()->drawForMe();
    }
}
void GameplayScreen::stepGamer(Gamer *first, Gamer *second)
{
    bool hit = true;
    while (hit  && second->getCntShip() > 0)
    {
        update(first->getnameGamer());
        std::cout <<first->getnameGamer()<<consts::RATIO_SIGN;
        Coordinate guess = first->fire();
        consts::StatesShot state = second->getField()->checkGuess(guess);
        if(state == consts::EMPTY)
            hit = false;
        first->addShot(guess,state);
        system("pause");
    }
}

bool GameplayScreen::play()
{
    bool nowA = true;
    while (gamer1->getCntShip() > 0 && gamer2->getCntShip() > 0)
    {
        if(nowA)
        {
            stepGamer(gamer1, gamer2);
            nowA = false;
        }
        else
        {
            stepGamer(gamer2,gamer1);
            nowA = true;
        }
    }
    if (gamer1->getCntShip() > 0)
    {
        std::cout <<gamer1->getnameGamer()<<consts::WIN_LOCAL<< std::endl;
        return true;
    }
    else
    {
        std::cout <<gamer2->getnameGamer()<<consts::WIN_LOCAL << std::endl;
        return false;
    }
}


GameplayScreen::~GameplayScreen()
{
    delete gamer1;
    delete gamer2;
}

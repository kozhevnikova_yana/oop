#pragma once

#include <string>
#include <memory>
#include <iostream>
#include "Game/consts.h"
#include "Game/GameConfiguration.h"
#include "Player/Gamer.h"
#include "Player/Human.h"
#include "Player/RandomComputer.h"
#include "Player/CleverComputer.h"
#include "Player/Human.h"

class GameplayScreen
{
private:
    Gamer *gamer1;
    Gamer *gamer2;

    void stepOneGamer(Gamer *first, Gamer *second);
public:
    GameplayScreen();
    ~GameplayScreen();
    void init(GameConfiguration &config);
    void getShips();

    bool play();
};